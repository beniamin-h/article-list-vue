import axios from 'axios';

import { Categories } from '../../src/store/consts';
import Articles from '../../src/services/articles';

describe('Service articles', () => {
  describe('list', () => {
    let fakeResponse;
    let getSpy;
    let getThenSpy;

    beforeEach(() => {
      getSpy = jasmine.createSpyObj('get', ['then']);
      getThenSpy = jasmine.createSpyObj('then', ['catch']);
      fakeResponse = { data: { articles: jasmine.createSpy('fakeData') }};
      getSpy.then
        .and.callFake((fn) => {
          fn(fakeResponse);
          return getThenSpy;
        });
      spyOn(axios, 'get').and.returnValue(getSpy);
    });

    it('should call get on axios with fashion url', () => {
      Articles.list(Categories.fashion);
      expect(axios.get).toHaveBeenCalledWith('//localhost:6010/articles/fashion');
    });

    it('should call get on axios with sport url', () => {
      Articles.list(Categories.sport);
      expect(axios.get).toHaveBeenCalledWith('//localhost:6010/articles/sports');
    });

    it('should return response.data on get success', () => {
      Articles.list(Categories.sport)
        .then((data) => {
          expect(data).toBe(fakeResponse.data.articles);
        })
        .catch((error) => {
          expect(error).not.toBe(jasmine.any());
        });
    });

    it('should return error on get error', () => {
      const fakeError = jasmine.createSpy('fakeError');
      getSpy.then // disable success
        .and.callFake(() => getThenSpy);
      getThenSpy.catch // enable error
        .and.callFake((fn) => fn(fakeError));
      const resultPromise = Articles.list(Categories.fashion);
      resultPromise
        .then((data) => {
          expect(data).not.toBe(jasmine.any());
        })
        .catch((error) => {
          expect(error).toBe(fakeError);
        });
    });
  });
});
