import { concatArticles, sortArticlesByDate } from '../../src/store';

describe('Store', () => {
  describe('concatArticles', () => {
    it('returns a flat array', () => {
      const nestedArray = [
        ['a', 'b', 'c'], ['d'], ['e', 'f'],
      ];
      const result = concatArticles(nestedArray);
      expect(result).toEqual(['a', 'b', 'c', 'd', 'e', 'f']);
    });

    it('returns an empty array for [] given', () => {
      const nestedArray = [];
      const result = concatArticles(nestedArray);
      expect(result).toEqual([]);
    });

    it('returns [a, b, c] for [[a, b, c]] given', () => {
      const nestedArray = [
        ['a', 'b', 'c'],
      ];
      const result = concatArticles(nestedArray);
      expect(result).toEqual(['a', 'b', 'c']);
    });
  });

  describe('sortArticlesByDate', () => {
    it('returns asc sorted articles for desc=false', () => {
      const result = sortArticlesByDate([{
        id: 'a',
        date: '12. october 2018',
      }, {
        id: 'b',
        date: '24. may 2018',
      }, {
        id: 'c',
        date: '5. october 2018',
      }], false);
      expect(result).toEqual([
        {
          id: 'b',
          date: '24. may 2018',
        }, {
          id: 'c',
          date: '5. october 2018',
        }, {
          id: 'a',
          date: '12. october 2018',
        },
      ]);
    });

    it('returns desc sorted articles for desc=true', () => {
      const result = sortArticlesByDate([{
        id: 'a',
        date: '12. october 2018',
      }, {
        id: 'b',
        date: '24. may 2018',
      }, {
        id: 'c',
        date: '5. october 2018',
      }], true);
      expect(result).toEqual([
        {
          id: 'a',
          date: '12. october 2018',
        }, {
          id: 'c',
          date: '5. october 2018',
        }, {
          id: 'b',
          date: '24. may 2018',
        },
      ]);
    });
  });
});
