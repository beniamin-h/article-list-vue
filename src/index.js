import Vue from 'vue';
import vuetify from './plugins/vuetify';

import App from './App';
import store from './store';

/* eslint-disable no-new */
new Vue({
  vuetify,
  store,
  el: '#app',
  components: { App },
  template: '<App/>',
});
