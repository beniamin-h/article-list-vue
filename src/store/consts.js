
// there will be more exports in the future - thus no default export
// eslint-disable-next-line import/prefer-default-export
export const Categories = Object.freeze({
  sport: Symbol('sport'),
  fashion: Symbol('fashion'),
});
