import Vue from 'vue';
import Vuex from 'vuex';

import Articles from '../services/articles';
import { Categories } from './consts';

Vue.use(Vuex);

export const concatArticles = (articleBunch) => (
  articleBunch.reduce((result, articles) => (
    result.concat(articles || [])
  ), [])
);

export const sortArticlesByDate = (articles, desc) => (
  articles.sort((articleA, articleB) => (
    (desc ? -1 : 1)
      * (new Date(articleA.date).getTime() - new Date(articleB.date).getTime())
  ))
);

export default new Vuex.Store({
  state: {
    articles: [],
    articleListLoaded: true,
    articleLoadingTimeout: 0,
    categories: [Categories.sport, Categories.fashion],
    descSortOrder: 0,
  },
  actions: {
    loadArticles({ commit, state, dispatch }) {
      Promise
        .all(state.categories.map((category) => Articles.list(category)))
        .then((articleBunch) => {
          commit('updateArticles', sortArticlesByDate(
            concatArticles(articleBunch), state.descSortOrder,
          ));
          commit('setArticleListLoaded', true);
        })
        .catch(() => {
          commit('updateArticles', []);
          commit('setArticleListLoaded', false);
        });
      dispatch('resetLoadArticlesTimer');
    },
    setCategories({ commit, dispatch }, categories) {
      commit('updateCategories', categories);
      dispatch('loadArticles');
    },
    resetLoadArticlesTimer({ state, dispatch }) {
      clearTimeout(state.articleLoadingTimeout);
      state.articleLoadingTimeout = setTimeout(() => {
        dispatch('loadArticles');
      }, 2000);
    },
    setDescSortOrder({ state, commit }, desc) {
      commit('updateDescSortOrder', desc);
      commit('updateArticles',
        sortArticlesByDate(state.articles, state.descSortOrder));
    },
  },
  mutations: {
    updateArticles(state, articles) {
      state.articles = articles;
    },
    setArticleListLoaded(state, loaded) {
      state.articleListLoaded = loaded;
    },
    updateCategories(state, categories) {
      state.categories = categories;
    },
    updateDescSortOrder(state, desc) {
      state.descSortOrder = desc;
    },
  },
});
