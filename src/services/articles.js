import axios from 'axios';
import { Categories } from '../store/consts';

const listUrl = (suffix) => `//localhost:6010/articles/${suffix}`;

const categoryToUrlSuffix = {
  [Categories.fashion]: 'fashion',
  [Categories.sport]: 'sports',
};

export default {

  list(category) {
    const suffix = categoryToUrlSuffix[category];
    return new Promise((resolve, reject) => {
      axios.get(listUrl(suffix))
        .then((response) => resolve(response.data.articles))
        .catch((error) => reject(error));
    });
  },

};
