I implemented the solution using Vue.js (+Vuex) and Vuetify. 
There are some tests in `spec/` directory. 
Interval to check for the new backend data is set to 2s.
I fixed names of months in server.js, as backend 
is the only proper place to do it and changing it on the fly
on frontend would be a really bad practice. 
The code is ESLint/airbnb compliant.